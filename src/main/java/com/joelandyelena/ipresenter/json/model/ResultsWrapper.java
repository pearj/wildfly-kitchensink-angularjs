package com.joelandyelena.ipresenter.json.model;

import java.util.List;

public class ResultsWrapper {

   private List<Song> songs;

   public List<Song> getSongs() {
      return this.songs;
   }

   public void setSongs(final List<Song> songs) {
      this.songs = songs;
   }

}
