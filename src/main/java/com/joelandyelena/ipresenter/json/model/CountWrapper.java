package com.joelandyelena.ipresenter.json.model;

public class CountWrapper {

   private Long count;

   public CountWrapper(final Long count) {
      super();
      this.count = count;
   }

   public Long getCount() {
      return this.count;
   }

   public void setCount(final Long count) {
      this.count = count;
   }
}
