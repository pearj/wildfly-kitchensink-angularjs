package com.joelandyelena.ipresenter.json.model;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class Verse {
   @Column(length = 5000)
   private String verse;

   @Column
   private String type;

   public Verse() {
   }

   public Verse(final String verse, final String type) {
      super();
      this.verse = verse;
      this.type = type;
   }

   public String getVerse() {
      return this.verse;
   }

   public void setVerse(final String verse) {
      this.verse = verse;
   }

   public String getType() {
      return this.type;
   }

   public void setType(final String type) {
      this.type = type;
   }

   @Override
   public String toString() {
      return "Verse [verse=" + this.verse + ", type=" + this.type + "]";
   }
}
