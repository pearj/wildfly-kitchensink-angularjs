package com.joelandyelena.ipresenter.json.model;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.OrderColumn;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@XmlRootElement
@Table(indexes = { @Index(columnList = "lastupdated") })
public class Song {

   @Id
   @GeneratedValue
   private Integer id;

   @Column(length = 500)
   private String title;

   @Column(length = 500)
   @ElementCollection(fetch = FetchType.EAGER)
   @OrderColumn
   private List<Author> author;

   @Column(length = 1000)
   private String copyright;

   @Column(length = 5000)
   @ElementCollection(fetch = FetchType.EAGER)
   @OrderColumn
   private List<Verse> lyrics;

   @Column
   private Integer ccli;

   @Column(columnDefinition = "timestamp with time zone not null")
   @Temporal(TemporalType.TIMESTAMP)
   private Date lastUpdated;

   public Song() {

   }

   public Integer getId() {
      return this.id;
   }

   public void setId(final Integer id) {
      this.id = id;
   }

   public String getTitle() {
      return this.title;
   }

   public void setTitle(final String title) {
      this.title = title;
   }

   public List<Author> getAuthor() {
      return this.author;
   }

   public void setAuthor(final List<Author> author) {
      this.author = author;
   }

   public String getCopyright() {
      return this.copyright;
   }

   public void setCopyright(final String copyright) {
      this.copyright = copyright;
   }

   public List<Verse> getLyrics() {
      return this.lyrics;
   }

   public void setLyrics(final List<Verse> lyrics) {
      this.lyrics = lyrics;
   }

   public Integer getCcli() {
      return this.ccli;
   }

   public void setCcli(final Integer ccli) {
      this.ccli = ccli;
   }

   public Date getLastUpdated() {
      return this.lastUpdated;
   }

   public void setLastUpdated(final Date lastUpdated) {
      this.lastUpdated = lastUpdated;
   }

   @Override
   public String toString() {
      return "Song [id=" + this.id + ", title=" + this.title + ", author=" + this.author + ", copyright="
             + this.copyright + ", lyrics=" + this.lyrics + ", ccli=" + this.ccli + ", lastUpdated=" + this.lastUpdated
             + "]";
   }

}
