package com.joelandyelena.ipresenter.json.model;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class Author {

   public Author() {

   }

   public Author(final String name) {
      super();
      this.name = name;
   }

   @Column(length = 500)
   private String name;

   public String getName() {
      return this.name;
   }

   public void setName(final String name) {
      this.name = name;
   }

   @Override
   public String toString() {
      return "Author [name=" + this.name + "]";
   }
}
