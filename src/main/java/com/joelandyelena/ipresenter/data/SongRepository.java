package com.joelandyelena.ipresenter.data;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import com.joelandyelena.ipresenter.json.model.Song;
import com.joelandyelena.ipresenter.json.model.Song_;

@Stateless
public class SongRepository {

   private static final int MAXIMUM_QUERY_RESULTS = 100;

   @Inject
   private EntityManager em;

   public List<Song> findUpdatedSongs(final Date lastUpdated,
                                      final Integer lastId) {

      final CriteriaBuilder cb = this.em.getCriteriaBuilder();
      final CriteriaQuery<Song> criteria = cb.createQuery(Song.class);
      final Root<Song> song = criteria.from(Song.class);

      criteria.select(song);

      this.addPredicates(criteria, cb, song, lastUpdated, lastId, false);

      final List<Song> resultList = this.em.createQuery(criteria).setMaxResults(MAXIMUM_QUERY_RESULTS).getResultList();

      return resultList;
   }

   public Long findUpdatedSongsCount(final Date lastUpdated,
                                     final Integer lastId) {
      final CriteriaBuilder cb = this.em.getCriteriaBuilder();
      final CriteriaQuery<Long> criteria = cb.createQuery(Long.class);
      final Root<Song> song = criteria.from(Song.class);

      criteria.select(cb.count(song));

      this.addPredicates(criteria, cb, song, lastUpdated, lastId, true);

      final Long count = this.em.createQuery(criteria).getSingleResult();

      return count;
   }

   private <T> void addPredicates(final CriteriaQuery<? extends T> criteria,
                                  final CriteriaBuilder cb,
                                  final Root<Song> song,
                                  final Date lastUpdated,
                                  final Integer lastId,
                                  final boolean count) {
      final Path<Date> colLastUpdated = song.get(Song_.lastUpdated);
      final Path<Integer> colSongId = song.get(Song_.id);

      // song.lastUpdated > lastUpdated OR (song.lastUpdated = lastUpdated AND song.id > lastId)
      final Predicate or = cb.or(cb.greaterThan(colLastUpdated, lastUpdated),
                                 cb.and(cb.equal(colLastUpdated, lastUpdated), cb.greaterThan(colSongId, lastId)));

      criteria.where(or);

      if (!count) {
         criteria.orderBy(cb.asc(colLastUpdated), cb.asc(colSongId));
      }

   }

   public Map<Integer, Song> findAllOrderedById() {
      final LinkedHashMap<Integer, Song> songs = new LinkedHashMap<Integer, Song>();

      final CriteriaBuilder cb = this.em.getCriteriaBuilder();
      final CriteriaQuery<Song> criteria = cb.createQuery(Song.class);
      final Root<Song> song = criteria.from(Song.class);
      criteria.select(song).orderBy(cb.asc(song.get(Song_.lastUpdated)), cb.asc(song.get(Song_.id)));

      final List<Song> resultList = this.em.createQuery(criteria).getResultList();
      for (final Song songObj : resultList) {
         songs.put(songObj.getId(), songObj);
      }

      return songs;
   }

   public void saveSong(final Song song) {
      song.setLastUpdated(new Date());
      if (song.getId() == null) {
         this.em.persist(song);
      }
      else {
         this.em.merge(song);
      }
   }

}
