package com.joelandyelena.ipresenter.rest;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletResponse;

/**
 * Allow all the REST services to be accessed on other domains.
 *
 * @author Joel
 *
 */
@WebFilter(filterName = "CORSFilter", urlPatterns = { "/*" })
public class CORSFilter implements Filter {

   @Override
   public void destroy() {
      // TODO Auto-generated method stub

   }

   @Override
   public void doFilter(final ServletRequest arg0,
                        final ServletResponse resp,
                        final FilterChain arg2) throws IOException, ServletException {
      // TODO Auto-generated method stub
      final HttpServletResponse httpResp = (HttpServletResponse) resp;
      httpResp.addHeader("Access-Control-Allow-Origin", "*");
      httpResp.addHeader("Access-Control-Allow-Headers", "origin, content-type, accept, authorization");
      httpResp.addHeader("Access-Control-Allow-Credentials", "true");
      httpResp.addHeader("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD");
      httpResp.addHeader("Access-Control-Max-Age", "1209600");

   }

   @Override
   public void init(final FilterConfig arg0) throws ServletException {
      // TODO Auto-generated method stub

   }

}
