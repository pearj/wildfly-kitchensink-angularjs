package com.joelandyelena.ipresenter.rest;

import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.joelandyelena.ipresenter.data.SongRepository;
import com.joelandyelena.ipresenter.json.model.CountWrapper;
import com.joelandyelena.ipresenter.json.model.ResultsWrapper;
import com.joelandyelena.ipresenter.json.model.Song;

@Path("/songs")
@RequestScoped
public class SongResourceRESTService {

   @Inject
   private SongRepository songRepository;

   @GET
   @Produces(MediaType.APPLICATION_JSON)
   public ResultsWrapper listSongsSinceDate(@QueryParam("last-updated")
                                            final String lastUpdatedStr,
                                            @QueryParam("last-id")
                                            final Integer lastId) throws ParseException {
      final Date lastUpdated = new Date(Long.parseLong(lastUpdatedStr));
      final List<Song> updatedSongs = this.songRepository.findUpdatedSongs(lastUpdated, lastId);
      final ResultsWrapper wrapper = new ResultsWrapper();
      wrapper.setSongs(updatedSongs);
      return wrapper;
   }

   @POST
   @Consumes(MediaType.APPLICATION_JSON)
   @Produces(MediaType.APPLICATION_JSON)
   public Response saveSong(final Song song) {
      this.songRepository.saveSong(song);
      return Response.ok().build();
   }

   @GET
   @Produces(MediaType.APPLICATION_JSON)
   @Path("/count")
   public CountWrapper listSongsSinceDateCount(@QueryParam("last-updated")
                                               final String lastUpdatedStr,
                                               @QueryParam("last-id")
                                               final Integer lastId) throws ParseException {
      final Date lastUpdated = new Date(Long.parseLong(lastUpdatedStr));
      final Long updatedSongs = this.songRepository.findUpdatedSongsCount(lastUpdated, lastId);
      return new CountWrapper(updatedSongs);
   }

   @GET
   @Path("/all")
   @Produces(MediaType.APPLICATION_JSON)
   public Map<Integer, Song> listAllSongsFromDb() {
      final Map<Integer, Song> allOrderedById = this.songRepository.findAllOrderedById();
      return allOrderedById;
   }

}
